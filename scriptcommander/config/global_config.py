
from scriptcommander.constants import tokens
from scriptcommander.config.pipeline.scripts import get_config as get_pipeline_config

# GLOBAL VARIABLES
FUNC_DATA = []
FUNC_UID_LOOKUP_DICT = {}

def init_func_data(package:str=tokens.PACKAGE_NUKE, userid="test"):
    global FUNC_DATA, FUNC_UID_LOOKUP_DICT

    model_data = get_pipeline_config()
    if package == tokens.PACKAGE_3DSMAX:
        #from scriptcommander.config.max.scripts import max_model_data
        #return model_data.extend(max_model_data)
        pass
    elif package == tokens.PACKAGE_NUKE:
        #from scriptcommander.config.nuke.scripts import nu
        pass

    FUNC_DATA = model_data
    FUNC_UID_LOOKUP_DICT = {item[tokens.FUNC_UID]: item for item in FUNC_DATA}



