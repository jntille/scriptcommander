import inspect
import hashlib

from PySide2.QtGui import QIcon
from PySide2.QtWidgets import QApplication

from docstring_parser import parse

from scriptcommander.constants import tokens
from scriptcommander.rc import icon_tokens
from unexQt.styling.utils import icons


def create_function_uid(func_data) -> QIcon:
    """uid is used to identify a function"""
    uid ="{title}{name}{params}".format(
        title=func_data[tokens.FUNC_TITLE],
        name=func_data[tokens.FUNC_OBJECT].__name__,
        params=func_data.get(tokens.FUNC_PARAMS, "")
    ).encode()
    return hashlib.sha256(uid).hexdigest()

def extend_with_additional_information(config:list):
    """collects additional information"""

    for func_dict in config:
        func_obj = func_dict.get(tokens.FUNC_OBJECT)

        # no user title set, generate one
        if tokens.FUNC_TITLE not in func_dict:
            func_dict[tokens.FUNC_TITLE] = func_obj.__name__.replace("_", " ")

        # add docstring information
        func_dict[tokens.FUNC_DOCSTRING] = parse(inspect.getdoc(func_obj))

        # adds uid
        func_dict[tokens.FUNC_UID] = create_function_uid(func_dict)

        # adds default icon if none set
        if not tokens.FUNC_ICON in func_dict:
            func_dict[tokens.FUNC_ICON] = icon_tokens.CLASS


    return config

def get_sanitized_config(config:list):
    """removes invalid entries from the config to avoid crashes"""
    for func_dict in config:
        # ENFORCED CHECKS (removes non-conform entries)

        func_obj = func_dict.get(tokens.FUNC_OBJECT)
        if func_obj is None: # must contain executable function
            config.remove(func_dict)
            continue
        else:
            # throw exception
            pass


        sig = inspect.signature(func_obj)
        
        for param_name, param_obj in sig.parameters.items():
            # match specified paramters against function signature
            if param_name not in func_dict[tokens.FUNC_PARAMS]: 
                config.remove(func_dict)
                break
            
            # widget returns type matches the functions type hints
            # ...
  
    return config

def get_config(config:list):
    return extend_with_additional_information(get_sanitized_config(config))