import inspect
from docstring_parser import parse

from scriptcommander.constants import tokens
from scriptcommander.config.utils import config_validation
from scriptcommander.widgets.auto_func_ui import widget_factory

from scriptcommander.rc import icon_tokens

# import functions here
from scriptcommander.config.pipeline.demo import demo1, demo2

# config set by the user
pipeline_model_data = [
    {
        tokens.FUNC_TITLE: "Ignite Fire",
        tokens.FUNC_SHORT_TITLE: "Ignite",
        tokens.FUNC_ICON: icon_tokens.ACCOUNT_TREE,
        tokens.FUNC_OBJECT: demo2,
        tokens.FUNC_PARAMS: {
            "isFlamable": widget_factory.checkbox_factory("flamable", False),
            "fuel_factor": widget_factory.int_spinboxslider_factory()
        }
    },
    {
        tokens.FUNC_TITLE: "Mark render as errorous",
        tokens.FUNC_OBJECT: demo1,
    },
    {
        tokens.FUNC_TITLE: "Submit to render farm",
        tokens.FUNC_OBJECT: demo1,
        tokens.FUNC_PARAMS: {}
    },
    {
        tokens.FUNC_TITLE: "Submit for review",
        tokens.FUNC_OBJECT: demo1,
        tokens.FUNC_PARAMS: {}
    },
    {
        tokens.FUNC_TITLE: "Notify artist",
        tokens.FUNC_OBJECT: demo1,
        tokens.FUNC_PARAMS: {}
    },
    {
        tokens.FUNC_TITLE: "Check  for updates",
        tokens.FUNC_OBJECT: demo1,
        tokens.FUNC_PARAMS: {}
    },
]

def get_config():
    return config_validation.get_config(pipeline_model_data)
