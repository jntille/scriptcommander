def demo1():
    """[summary]
    """
    print("demo1")

def demo2(isFlamable:bool, fuel_factor:int=10):
    """Set it all on fire

    Args:
        isFlamable (bool): Some description for this parameter
        fuel_factor (int, optional): Scales the number of explosives. Defaults to 10.
    """
    print("demo2 called with: isFlamable={}, fuel_factor={}".format(isFlamable, fuel_factor))


def demo3(pos_param:bool, kwarg1="test", kwarg2=12):
    """Delete the entire pipeline

    Args:
        pos_param (bool): [description]
        kwarg1 (str, optional): [description]. Defaults to "test".
        kwarg2 (int, optional): [description]. Defaults to 12.
    """
    print("demo3 called with: {}".format(locals()))
