# TOKENS for datastructure
FUNC_TITLE = "func_title"
FUNC_SHORT_TITLE = "func_short_title"
FUNC_UID = "func_uid"

FUNC_DOCSTRING = "func_docstring"
FUNC_ICON = "func_icon"

FUNC_OBJECT = "func_obj"
FUNC_PARAMS = "func_params"
#FUNC_CONTEXT = "func_context"

#TOOL TOKENS
PACKAGE_3DSMAX = "MAX"
PACKAGE_NUKE = "NK"

def get_all_packages():
    return [PACKAGE_3DSMAX, PACKAGE_NUKE]