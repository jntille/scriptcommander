import pathlib

SCRIPTCOMMANDER_ROOT = pathlib.Path(__file__).parent.parent
DATA_ROOT = SCRIPTCOMMANDER_ROOT / "data"
