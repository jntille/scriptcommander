from PySide2 import QtCore

# search widget 
TitleRole = QtCore.Qt.UserRole + 1
ShortDescriptionRole = QtCore.Qt.UserRole + 2
IconRole = QtCore.Qt.UserRole + 3
FuncDataRole = QtCore.Qt.UserRole + 4

# Bookmark
ShortTitleRole = QtCore.Qt.UserRole + 5
BookmarkDataRole = QtCore.Qt.UserRole + 6
