from PySide2 import QtCore

TitleRole = QtCore.Qt.UserRole + 1
DocstringRole = QtCore.Qt.UserRole + 2
ImageRole = QtCore.Qt.UserRole + 3
FuncObjRole = QtCore.Qt.UserRole + 4 
FuncParamRole = QtCore.Qt.UserRole + 5
FuncDataRole = QtCore.Qt.UserRole + 6
