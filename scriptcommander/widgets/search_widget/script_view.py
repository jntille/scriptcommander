
from PySide2 import QtWidgets, QtCore, QtGui

from scriptcommander.widgets.search_widget import search_model, search_delegate
from unexQt.widgets.UListView import USearchListView

from scriptcommander.constants.roles import TitleRole, FuncDataRole

class ScriptSearchView(USearchListView.USearchListView):
    def __init__(self, model_data=[], parent=None):
        super(ScriptSearchView, self).__init__(parent)

        # model
        src_model = search_model.ScriptSearchModel(model_data)
        self.set_source_model(src_model)

        # delegate
        self.setItemDelegate(search_delegate.ScriptSearchDelegate())

        self.setDragEnabled(True)
        self.setAcceptDrops(False)
        self.set_max_items_to_display(5)

        self.get_proxy_model().setFilterRole(TitleRole)

        
