from PySide2 import QtWidgets, QtCore, QtGui

from unexQt.styling import unexQt
from unexQt.widgets.ULineEdit import URegexSearchLine

from scriptcommander.widgets.search_widget import script_view 
from scriptcommander.widgets.auto_func_ui.function_exec import execute_function

class SearchWidget(QtWidgets.QWidget):
    def __init__(self, model_data=[], parent=None):
        super(SearchWidget, self).__init__(parent)
        # self.setStyleSheet(unexQt.load_stylesheet_pyside2())

        self.main_layout = QtWidgets.QVBoxLayout()
        self.main_layout.setContentsMargins(0,0,0,0)
        self.main_layout.setAlignment(QtCore.Qt.AlignTop)
        self.setLayout(self.main_layout)
   
        self.regex_search = URegexSearchLine.URegexSearchLine()
        self.main_layout.addWidget(self.regex_search)  

        self.script_view = script_view.ScriptSearchView(model_data)
        self.main_layout.addWidget(self.script_view)

        self.setSizePolicy(
            QtWidgets.QSizePolicy.Policy.Preferred, 
            QtWidgets.QSizePolicy.Policy.Fixed
        )

        # wire lineedit and listview together with signals
        self.regex_search.regexChanged.connect(self.handle_regex_change)
        self.regex_search.upDownPressed.connect(self.script_view.move_index_by_pressed_key)

        self.regex_search.searchExecuted.connect(lambda: execute_function(self.script_view.currentIndex()))
        self.script_view.activated.connect(lambda index: execute_function(index))
    
    def handle_regex_change(self, regex:QtCore.QRegularExpression):
        self.script_view.set_proxy_filter(regex)
        self.updateGeometry()