
from PySide2 import QtWidgets, QtCore, QtGui
from inspect import getdoc

from scriptcommander.constants import tokens
from unexQt.widgets.UDragDropListModel import UDragDropListModel

from scriptcommander.constants.roles import TitleRole, ShortDescriptionRole, IconRole, FuncDataRole

# ICONS
import scriptcommander.rc.pyside2_icons_rc as icon_rc
from scriptcommander.rc import icon_tokens
from unexQt.styling.utils import icons

class ScriptSearchModel(UDragDropListModel.UDragDropListModel):
    def __init__(self, model_data=[], parent=None):
        super(ScriptSearchModel, self).__init__(parent)
        self.set_model_data(model_data)

    def data(self, index, role):
        # translate index to dict key 
        if index.isValid():
            func_data = self.model_data[index.row()]
            
            if role == TitleRole:
                return func_data.get(tokens.FUNC_TITLE)
            elif role == ShortDescriptionRole:
                parsed_docstring = func_data.get(tokens.FUNC_DOCSTRING, None)
                if parsed_docstring:
                    return parsed_docstring.short_description                
            elif role == IconRole:
                return icons.get_icon(func_data[tokens.FUNC_ICON])
            elif role == FuncDataRole:
                return func_data
    
    def mimeTypes(self):
        """Returns the list of allowed MIME types"""
        return ["application/x-scriptcommander-drag-drop"]

    def dropMimeData(self, data:QtCore.QMimeData, action:QtCore.Qt.DropAction, row, column, parent):
        pass

    def serialize_item_to_stream(self, index, stream:QtCore.QDataStream) -> QtCore.QDataStream:
        func_data = index.data(FuncDataRole)
        stream.writeString(func_data[tokens.FUNC_UID])
        stream.writeString(func_data.get(tokens.FUNC_SHORT_TITLE, ""))

    def deserialize_item_from_stream(self, stream:QtCore.QDataStream):
        pass

