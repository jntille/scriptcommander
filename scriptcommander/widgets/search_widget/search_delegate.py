from PySide2 import QtWidgets, QtCore, QtGui

from unexQt.widgets.UListView import UAbstractITDDelegate
from scriptcommander.constants.roles import TitleRole, ShortDescriptionRole

class ScriptSearchDelegate(UAbstractITDDelegate.UAbstractITDDelegate):
    def __init__(self):
        super(ScriptSearchDelegate, self).__init__()

    # overrides to retrieve data from our custom search model
    def get_description(self, index):
        return index.data(ShortDescriptionRole)

    def get_title(self, index):
        return index.data(TitleRole)