from PySide2 import QtCore, QtGui, QtWidgets

from scriptcommander.constants.roles import ShortTitleRole
from scriptcommander.widgets.auto_func_ui.function_exec import execute_function

class BookmarkItemRightClickMenu(QtWidgets.QMenu):
    def __init__(self, parent=None):
        super(BookmarkItemRightClickMenu, self).__init__(parent)
        self.rename_action = self.addAction("rename")
        self.delete_action = self.addAction("delete")

class BookmarkListView(QtWidgets.QListView):
    def __init__(self, parent=None):
        super(BookmarkListView, self).__init__(parent)

        # members
        self.delegate = QtWidgets.QStyledItemDelegate(self)
        self.setItemDelegate(self.delegate)
        self.item_right_click_menu = BookmarkItemRightClickMenu()

        # drag drop 
        self.setAcceptDrops(True)
        self.setDragEnabled(True)
        self.setDropIndicatorShown(True)

        # item related
        self.setViewMode(QtWidgets.QListView.ViewMode.ListMode)
        self.setFlow(QtWidgets.QListView.Flow.LeftToRight)
        self.setUniformItemSizes(True)

        self.setSelectionMode(QtWidgets.QListView.SelectionMode.SingleSelection)
        self.setSelectionBehavior(QtWidgets.QListView.SelectionBehavior.SelectItems)

        # size related
        self.setFocusPolicy(QtCore.Qt.FocusPolicy.NoFocus)
        self.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.setSizePolicy(
            QtWidgets.QSizePolicy.Policy.Preferred,
            QtWidgets.QSizePolicy.Policy.Fixed
        )
        self.setSizeAdjustPolicy(QtWidgets.QListView.SizeAdjustPolicy.AdjustToContents)
        self.setResizeMode(QtWidgets.QListView.ResizeMode.Adjust)

        # nav related
        self.setTabKeyNavigation(False)

        # signals
        self.activated.connect(execute_function)

    def contextMenuEvent(self, event):
        gp = event.globalPos()
        lp = self.viewport().mapFromGlobal(gp)
        index = self.indexAt(lp)

        if index.isValid():
            action = self.item_right_click_menu.exec_(gp)
            # rename item
            if action == self.item_right_click_menu.rename_action:
                text, accepted = QtWidgets.QInputDialog.getText(
                    self,
                    "set short title",
                    "Rename to: ",
                    text=index.data(ShortTitleRole),
                )
                if accepted and text != "":
                    self.model().rename_short_title(index, text)

            # delete item
            if action == self.item_right_click_menu.delete_action:
                self.model().removeRows(index.row(), 1, index)

    def viewOptions(self) -> QtWidgets.QStyleOptionViewItem:
        option = super(BookmarkListView, self).viewOptions()
        option.decorationPosition = QtWidgets.QStyleOptionViewItem.Position.Top
        option.decorationAlignment = QtCore.Qt.AlignCenter
        option.displayAlignment = QtCore.Qt.AlignCenter
        option.decorationSize = QtCore.QSize(22,22)
        option.textElideMode = QtCore.Qt.ElideRight
        return option
