from PySide2 import QtWidgets, QtCore, QtGui

from unexQt.widgets.UDragDropListModel.UDragDropListModel import UDragDropListModel
from unexQt.styling.utils import icons

import scriptcommander.config.global_config
from scriptcommander.constants.roles import ShortTitleRole, IconRole, FuncDataRole, BookmarkDataRole
from scriptcommander.constants import tokens

class BookmarkListModel(UDragDropListModel):
    """This model only stores a bookmarked function's unique ID and short title to allow for on-disk serialization.
    This information is used to lookup the referenced function
    """

    def __init__(self, parent=None):
        super(BookmarkListModel, self).__init__(parent)
        self.set_allow_drop_on_item(False)
    
    def data(self, index, role):
        if index.isValid():
            item_data = self.model_data[index.row()]
            func_data = scriptcommander.config.global_config.FUNC_UID_LOOKUP_DICT[item_data[tokens.FUNC_UID]]

            if role == ShortTitleRole:
                custom_short_title = item_data.get(
                    tokens.FUNC_SHORT_TITLE,
                    func_data.get(tokens.FUNC_SHORT_TITLE, "None")
                )
                return custom_short_title

            if role == IconRole:
                return icons.get_icon(func_data[tokens.FUNC_ICON])


            if role == BookmarkDataRole:
                return item_data

            if role == FuncDataRole:
                return func_data

            # temp as long as bookmark doesn't use a custom delegate
            if role == QtCore.Qt.DisplayRole:
                custom_short_title = item_data.get(tokens.FUNC_SHORT_TITLE, "")
                if custom_short_title == "":
                    return func_data.get(tokens.FUNC_SHORT_TITLE, "None")
                else:
                    return custom_short_title

            if role == QtCore.Qt.DecorationRole:
                return icons.get_icon(func_data[tokens.FUNC_ICON])

            if role == QtCore.Qt.ToolTipRole:
                parsed_docstring = item_data.get(tokens.FUNC_DOCSTRING, None)
                if parsed_docstring:
                    return parsed_docstring.short_description
            
    def mimeTypes(self):
        return ["application/x-scriptcommander-drag-drop"]

    def dropMimeData(self, data:QtCore.QMimeData, action:QtCore.Qt.DropAction, row, column, parent):
        # Check for unsupported drops
        if action not in [QtCore.Qt.CopyAction, QtCore.Qt.MoveAction]:
            return False
        if not (data.hasFormat(self.mimeTypes()[0])):
            return False
        
        indexes, decoded_items, is_internal_drop = self.decode_data(data)
        is_drop_on_item = True if (parent.isValid() and row == -1 and column == -1) else False

        # if target row is an invalid model index, insert at the back of the list
        if not self.index(row, column).isValid():
            row = self.rowCount(QtCore.QModelIndex())

        if is_internal_drop:
            # treat interal drops as move
            self.insert_rows_with_data(row, decoded_items)

            # removal procedure is a bit tricky, because:
            # items with an index smaller than target_row maintain their original index for removal while influencing those after the target_row
            # items with an index greater than the target row influence their own removal index
            remove_indexes = [(src_row + len(decoded_items), src_col) if row < src_row else (src_row, src_col) for src_row, src_col in indexes]
            for remove_row, remove_col in remove_indexes:
                self.removeRow(remove_row, parent)
        else:
            # external drops add a new copy
            self.insert_rows_with_data(row, decoded_items)
        return True

    def serialize_item_to_stream(self, index, stream:QtCore.QDataStream):
        item_data = index.data(BookmarkDataRole)
        stream.writeString(item_data[tokens.FUNC_UID])
        stream.writeString(item_data.get(tokens.FUNC_SHORT_TITLE, ""))


    def deserialize_item_from_stream(self, stream:QtCore.QDataStream) -> dict:
        item_data = {
            tokens.FUNC_UID: stream.readString(),
            tokens.FUNC_SHORT_TITLE: stream.readString()
        }
        return item_data
    
    def rename_short_title(self, index:QtCore.QModelIndex, title:str):
        item_data = self.model_data[index.row()] 
        item_data[tokens.FUNC_SHORT_TITLE] = title
        self.model_data[index.row()] = item_data
        self.modelDataHasChanged.emit()