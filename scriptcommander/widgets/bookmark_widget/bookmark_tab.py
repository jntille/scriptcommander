import json

from PySide2 import QtCore, QtGui, QtWidgets

from unexQt.styling import unexQt
from unexQt.widgets.UTabWidget.UTabWidget import URenameableTabWidget

from scriptcommander.config import global_config
from scriptcommander.data import utils
from scriptcommander.constants import tokens
from scriptcommander.widgets.bookmark_widget import bookmark_view, bookmark_model


class BookmarkTabWidget(URenameableTabWidget):
    def __init__(self, user, dcc, parent=None):
        super(BookmarkTabWidget, self).__init__(parent)
        # self.setStyleSheet(unexQt.load_stylesheet_pyside2())

        # members 
        self._file = utils.get_bookmark_json_file(user, dcc)

        # size related
        self.setSizePolicy(
            QtWidgets.QSizePolicy.Policy.Preferred,
            QtWidgets.QSizePolicy.Policy.Fixed
        )
        self.setUsesScrollButtons(False)

        # signals
        self.tabAdded.connect(self.write_bookmarks_to_file)
        self.tabBar().tabRenamed.connect(self.write_bookmarks_to_file)
        self.tabBar().tabMoved.connect(self.write_bookmarks_to_file)
    
        # initialize
        self.load_bookmarks_from_file(self._file)

    # override
    def on_tab_close_requested(self, index):
        super(BookmarkTabWidget, self).on_tab_close_requested(index)
        self.write_bookmarks_to_file()
        
    def load_bookmarks_from_file(self, file):
        with open(file, "r") as f:
            try:
                data = json.load(f)
                if data:
                    # validate if bookmarked functions still exist
                    for tab_text, model_data in data.items():
                        for index, func_data in enumerate(model_data):
                            if not (func_data[tokens.FUNC_UID] in global_config.FUNC_UID_LOOKUP_DICT):
                                model_data.pop(index)
                else:
                    data = {"default": []}

            # fallback if faulty or empty format
            except json.JSONDecodeError:
                data = {"default": []}
        
        # setup tabs
        for tab_text, model_data in data.items():
            self.addTab(self.create_page_widget(model_data), tab_text)
    
    def write_bookmarks_to_file(self):
        """serialize data from all tabs and write to file"""
        data = {}
        for tab_index in range(self.tabBar().count()):
            tab_text = self.tabBar().tabText(tab_index)
            data[tab_text] = self.widget(tab_index).model().get_model_data()

        with open(self._file, 'w') as f:
            json.dump(data, f, indent=4)
    
    def create_page_widget(self, model_data=[]):
        tab_view = bookmark_view.BookmarkListView()
        tab_model = bookmark_model.BookmarkListModel(tab_view)
        tab_model.set_model_data(model_data)
        tab_view.setModel(tab_model)

        tab_model.modelDataHasChanged.connect(self.write_bookmarks_to_file)
        tab_view.destroyed.connect(lambda: print("view destroyed"))
        tab_model.destroyed.connect(lambda: print("model destroyed"))

        return tab_view
