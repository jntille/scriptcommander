from PySide2 import QtCore, QtGui, QtWidgets

from scriptcommander.widgets.auto_func_ui.param_widget_interface import ParamWidgetInterface

class IntSpinboxSlider(QtWidgets.QWidget, ParamWidgetInterface):
    def __init__(self, config, parent=None):
        super(IntSpinboxSlider, self).__init__(parent)

        self.lay = QtWidgets.QHBoxLayout()
        self.lay.setContentsMargins(0,0,0,0)
        self.setLayout(self.lay)

        self.spinbox = QtWidgets.QSpinBox()  
        self.slider = QtWidgets.QSlider(QtCore.Qt.Horizontal)
        self.lay.addWidget(self.slider)
        self.lay.addWidget(self.spinbox)

        self.setConfig(config)
        self.spinbox.setAlignment(QtCore.Qt.AlignHCenter | QtCore.Qt.AlignVCenter)

        # SIGNALS
        self.slider.sliderMoved.connect(self.spinbox.setValue)
        self.spinbox.valueChanged.connect(self.slider.setValue)

    def setConfig(self, config):
        for key, value in config.items():
            if key == "range":
                self.slider.setRange(value[0],value[1])

    def get_value(self):
        return self.spinbox.value()

    def set_value(self, value):
        return self.spinbox.setValue(value)

class DoubleSpinboxSlider(QtWidgets.QWidget, ParamWidgetInterface):
    def __init__(self, config, parent=None):
        super(DoubleSpinboxSlider, self).__init__(parent)

        self.lay = QtWidgets.QHBoxLayout()
        self.lay.setContentsMargins(0,0,0,0)
        self.setLayout(self.lay)

        self.spinbox = QtWidgets.QDoubleSpinBox()  
        self.slider = QtWidgets.QSlider(QtCore.Qt.Horizontal)
        self.lay.addWidget(self.slider)
        self.lay.addWidget(self.spinbox)

        self.setConfig(config)
        self.spinbox.setAlignment(QtCore.Qt.AlignHCenter | QtCore.Qt.AlignVCenter)

        # SIGNALS
        self.slider.sliderMoved.connect(self.spinbox.setValue)
        self.spinbox.valueChanged.connect(self.slider.setValue)

    def setConfig(self, config):
        for key, value in config.items():
            if key == "range":
                self.slider.setRange(value[0],value[1])

    def get_value(self):
        return self.spinbox.value()

    def set_value(self, value):
        return self.spinbox.setValue(value)