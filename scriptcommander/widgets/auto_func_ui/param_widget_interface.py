from abc import ABCMeta, abstractmethod

class ParamWidgetInterface:
    @abstractmethod
    def setConfig(self, config):
        raise NotImplementedError

    @abstractmethod
    def get_value(self):
        raise NotImplementedError

    @abstractmethod
    def set_value(self, value:bool):
        raise NotImplementedError