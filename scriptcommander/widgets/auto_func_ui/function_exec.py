import inspect

from PySide2 import QtWidgets, QtCore, QtGui

from unexQt.widgets.UDialog import UDialog
from unexQt.styling.utils import icons
from unexQt.widgets.ULabel import ULabelLine

from scriptcommander.constants import tokens
from scriptcommander.constants.roles import FuncDataRole


class FunctionExecWidget(QtWidgets.QWidget):
    def __init__(self, func_data:dict):
        super(FunctionExecWidget, self).__init__()
        # members
        self.params = {} # maps the functions parameters to the respective input widgets
        self._func_data = func_data
        self._func_object = self._func_data[tokens.FUNC_OBJECT]
        self._func_signature = inspect.signature(self._func_object)
        self._func_docstring = self._func_data[tokens.FUNC_DOCSTRING]

        # layout
        self.main_layout = QtWidgets.QVBoxLayout()
        self.main_layout.setAlignment(QtCore.Qt.AlignTop)
        self.setLayout(self.main_layout)

        self.func_sign_label = ULabelLine.ULabelLine()
        self.func_sign_label.setTextFormat(QtCore.Qt.TextFormat.MarkdownText)
        self.func_sign_label.setText(
            "**{func_name}** ({parameters})".format(
                func_name=self._func_object.__name__,
                parameters=", ".join([param_name for param_name, param_obj in self._func_signature.parameters.items()])
            )
        )
        self.func_sign_label.setToolTip(inspect.getdoc(self._func_object)) # TODO: pretty formatting as richtext?
        self.main_layout.addWidget(self.func_sign_label)

        # param box
        self.param_group_box = QtWidgets.QGroupBox()
        self.main_layout.addWidget(self.param_group_box)
        self.param_layout = QtWidgets.QFormLayout()
        self.param_group_box.setLayout(self.param_layout)

        # build parameter widgets
        self.build_widgets(self._func_data[tokens.FUNC_PARAMS])

    def build_widgets(self, parameters:dict):
        param_docstrings = {param.arg_name: param.description for param in self._func_docstring.params}
        
        for keyword, widget_factory in parameters.items():
            widget = widget_factory()
            widget.setToolTip(param_docstrings[keyword])

            # initialize defaults
            default = self._func_signature.parameters.get(keyword).default 
            if default is not inspect.Parameter.empty:
                widget.set_value(default)
            
            # add to layout
            if isinstance(widget, QtWidgets.QCheckBox):
                self.param_layout.addRow("", widget)
            else:
                self.param_layout.addRow("{}: ".format(keyword), widget)

            # add to dict
            self.params[keyword] = widget

    def get_user_input_values(self):
        return {parameter: widget.get_value() for parameter, widget in self.params.items()} if bool(self.params) else {}

    def execute(self):
        user_input_values = self.get_user_input_values()
        positional_only = []
        positional_or_keyword = []
        args = []
        keyword_only = {}
        kwargs = {}

        # differentiate between positional and keyword arguments
        for param in self._func_signature.parameters.values():
            if param.kind == param.POSITIONAL_ONLY:
                positional_only.append(user_input_values[param.name])
            elif param.kind == param.POSITIONAL_OR_KEYWORD:
                positional_or_keyword.append(user_input_values[param.name])
            elif param.kind == param.VAR_POSITIONAL:
                args.append(user_input_values[param.name])
            elif param.kind == param.KEYWORD_ONLY:
                keyword_only[param] = user_input_values[param.name]
            elif param.kind == param.VAR_KEYWORD:
                kwargs[param] = user_input_values[param.name]

        # call function with arguments
        self._func_object(*positional_only, *positional_or_keyword, *args, **keyword_only, **kwargs)

class FunctionExecDialog(QtWidgets.QDialog):
    def __init__(self, func_data, parent=None):
        super(FunctionExecDialog, self).__init__(parent)
        self.setWindowTitle(func_data[tokens.FUNC_TITLE])
        self.setWindowIcon(icons.get_icon(func_data[tokens.FUNC_ICON]))

        self.layout = QtWidgets.QVBoxLayout()
        self.setLayout(self.layout)

        self.func_widget = FunctionExecWidget(func_data)
        self.layout.addWidget(self.func_widget)

        self.buttonBox = QtWidgets.QDialogButtonBox(QtWidgets.QDialogButtonBox.Ok | QtWidgets.QDialogButtonBox.Cancel)
        self.layout.addWidget(self.buttonBox)

        self.buttonBox.accepted.connect(self.accept)
        self.buttonBox.rejected.connect(self.reject)
        
    def accept(self):
        self.func_widget.execute()
        return super().accept()

    def reject(self):
        return super().reject()

def execute_function(index:QtCore.QModelIndex):
    if index.isValid():
        func_data = index.data(FuncDataRole)

        # if function requires user input for parameters, autogenerate Dialog
        if tokens.FUNC_PARAMS in func_data and len(func_data[tokens.FUNC_PARAMS]) > 0:
            FunctionExecDialog(func_data).exec_()
        else:
            func_data[tokens.FUNC_OBJECT]()
