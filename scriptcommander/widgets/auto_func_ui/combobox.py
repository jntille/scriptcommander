from PySide2 import QtCore, QtGui, QtWidgets

from scriptcommander.widgets.auto_func_ui.param_widget_interface import ParamWidgetInterface

class Combobox(QtWidgets.QComboBox, ParamWidgetInterface):
    def __init__(self, config):
        super(Combobox, self).__init__()
        self.setConfig(config)

    def setConfig(self, config):
        self.setModel(ComboboxModel(config))
    
    def get_value(self):
        return self.currentText()

    def set_value(self, value:str):
        return self.setCurrentText(value)

class ComboboxModel(QtCore.QAbstractListModel, ParamWidgetInterface):
    def __init__(self, config):
        super(ComboboxModel, self).__init__()
        self.setConfig(config)

    def setConfig(self, config):
        for key, value in config.items():
            if key == "items":
                self.model = value

    def data(self, index, role):
        if role == QtCore.Qt.DisplayRole:
            return self.model[index.row()]

    def rowCount(self, index):
        return len(self.model)
