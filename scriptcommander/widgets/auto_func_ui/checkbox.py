from PySide2 import QtCore, QtGui, QtWidgets

from scriptcommander.widgets.auto_func_ui.param_widget_interface import ParamWidgetInterface

class Checkbox(QtWidgets.QCheckBox, ParamWidgetInterface):
    def __init__(self, config):
        super(Checkbox, self).__init__()
        self.setConfig(config)

    def setConfig(self, config):
        for key, value in config.items():
            if key == "label":
                self.setText(value)
            elif key == "checked":
                self.setChecked(value)

    def get_value(self):
        return self.isChecked()

    def set_value(self, value:bool):
        return self.setChecked(value)
