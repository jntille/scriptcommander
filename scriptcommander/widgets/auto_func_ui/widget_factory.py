# Notes:
# These factories return a partial object from which a UI can be constructed 
from functools import partial

from scriptcommander.widgets.auto_func_ui import checkbox, combobox, spinboxslider

def checkbox_factory(label:str="", checked:bool=False):
    return partial(checkbox.Checkbox, locals())
    
def combobox_factory(items:list=[]):
    return partial(combobox.Combobox, locals())

def int_spinboxslider_factory(range=(0,100)):
    return partial(spinboxslider.IntSpinboxSlider, locals())

def float_spinboxslider_factory(range=(0,100)):
    return partial(spinboxslider.DoubleSpinboxSlider, locals())