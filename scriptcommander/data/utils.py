import pathlib

from scriptcommander.constants import tokens, directories

# helper functions 
def get_bookmark_json_file(userid, software_package) -> pathlib.Path:
    bookmark_file = directories.DATA_ROOT / userid / f"{software_package}_bookmarks.json"
    user_dir = bookmark_file.parent
    if not user_dir.exists():
        user_dir.mkdir()
    if not bookmark_file.exists():
        bookmark_file.touch()
    
    return bookmark_file