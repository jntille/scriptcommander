import pathlib
import json
import argparse
import sys
import shutil
import os

from unexQt.styling.utils import color_schemes
from unexQt.styling.utils import rc

def generate_icon_tokens_file(tokens_file:pathlib.Path, svg_dir:pathlib.Path, rc_prefix:str, rc_subdir:str):
    """assumes that svgs have been compiled to a rc file"""
    ICON_TOKEN_TEMPLATE = "{IDENTIFIER} = ':/{prefix}/{sub_dir}/{default_png_file}'\n"

    with open(str(tokens_file), 'w') as f:
        token_lines = [ICON_TOKEN_TEMPLATE.format(
            IDENTIFIER=svg_file.stem.upper(),
            prefix=rc_prefix,
            sub_dir=rc_subdir,
            default_png_file=svg_file.stem + ".png",
            newline=os.sep
        ) for svg_file in svg_dir.glob("*.svg")]

        f.writelines(token_lines)

def main(arguments):
    # variables
    RC_DIR = pathlib.Path(__file__).parent.parent / "scriptcommander" / "rc"
    svg_dir = RC_DIR / "svg"
    img_dir = RC_DIR / "img"
    qrc_file = RC_DIR / "icons.qrc"
    icon_tokens_file = RC_DIR / "icon_tokens.py"
    prefix = "scriptcommander"
    color_scheme_file = color_schemes.COLOR_SCHEMES_DIR / "dark.json"

    # remove old state
    if img_dir.exists():
        shutil.rmtree(str(img_dir))
    img_dir.mkdir()
    
    if icon_tokens_file.exists():
        icon_tokens_file.unlink()
    
    if qrc_file.exists():
        qrc_file.unlink()

    # convert svg's to pngs according to the color scheme
    with open(color_scheme_file, "r") as f:
        color_scheme = json.load(f)
    rc.create_images(svg_dir, img_dir, color_scheme)

    # create icons qrc file
    content = rc.generate_qrc_string_for_icons(img_dir, "img",prefix)
    rc.write_qrc_file(content, qrc_file)
    
    # temp parse
    parser = argparse.ArgumentParser(description=__doc__,
                                     formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('--compile_for',
                        default='pyside2',
                        choices=['pyqt', 'pyqt5', 'pyside', 'pyside2', 'qtpy', 'pyqtgraph', 'qt', 'qt5', 'all'],
                        type=str,
                        help="Choose which one would be generated.")
    args = parser.parse_args(arguments)

    # compile qrc into importable resource
    rc.create_rcc(RC_DIR, qrc_file, args.compile_for)
    
    # generate icon tokens
    generate_icon_tokens_file(icon_tokens_file, svg_dir, prefix, "img")

if __name__ == '__main__':
    sys.exit(main(sys.argv[1:]))