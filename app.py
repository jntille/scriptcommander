
from PySide2 import QtWidgets, QtCore, QtGui

from unexQt.styling import unexQt

import scriptcommander.config.global_config
from scriptcommander.constants import tokens
from scriptcommander.widgets.search_widget import search_widget
from scriptcommander.widgets.bookmark_widget import bookmark_tab

if __name__ == "__main__":
    scriptcommander.config.global_config.init_func_data()
    app = QtWidgets.QApplication([])
    app.setStyleSheet(unexQt.load_stylesheet_pyside2())

    # search
    search_widget = search_widget.SearchWidget(scriptcommander.config.global_config.FUNC_DATA)
    search_widget.show()

    # tab 
    bookmark_widget = bookmark_tab.BookmarkTabWidget("test", tokens.PACKAGE_NUKE)
    bookmark_widget.show()

    app.exec_()